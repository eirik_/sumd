#!/usr/bin/env bash

error_exit(){
    echo "${PROGNAME}: ${1:-"Unknown error"}, aborting" 1>&2
    exit 1
}

if [[ $(id -u) != 0 ]]; then
  error_exit "$LINENO: You must be the superuser to run this script."
fi

echo "Creating system user 'sumd'"
adduser -q --system --no-create-home --group --disabled-password --disabled-login sumd || error_exit "$LINENO: Unable to create user 'sumd'"

echo "Adding user 'sumd' to group 'adm'"
usermod -aG adm sumd || error_exit "$LINENO: Unable to add user 'sumd' to group 'adm'"

echo "Creating folder '/etc/sumd/'"
mkdir /etc/sumd || error_exit "$LINENO: Unable to create folder '/etc/sumd/'"

echo "Creating folder '/var/local/sumd/"
mkdir /var/local/sumd || error_exit "$LINENO: Unable to create folder '/var/local/sumd'"
chown sumd:sumd /var/local/sumd

echo "Copying configuration file to '/etc/sumd/'"
cp sumd.conf /etc/sumd/ || error_exit "$LINENO: Unable to copy 'sumd.conf' to '/etc/sumd/'"

echo "Copying service file to '/etc/systemd/system'"
cp sumd.service /etc/systemd/system/ || error_exit "$LINENO: Unable to copy 'sumd.service' to '/etc/systemd/system/'"

echo "Copying script file to '/usr/local/sbin'"
cp sumd /usr/local/sbin/ || error_exit "$LINENO: Unable to copy 'sumd' to '/usr/local/sbin"