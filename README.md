**sumd** [-h|--help\] [-r|--remote] \[-s|--service]  
Version: 0.1

Description
-----------

**sumd** is a tool written in Bash for generating security summary
reports. It uses the systemd journal and GNU coreutils to get security
related information. It can be used as a command line tool that prints
to standard output, or run as a systemd service which sends the report
to a specified e-mail address.  
When run as a command line tool it generates the report based on log
data from the last 24 hours. When run as a service it will use the data
since the last time it was run, or since it was started, but it will not
run more than once per day. When run as a service, last run time
persists through reboots. And which users that has logged in is not sent
in the e-mail report, this is a security measure to prevent leaking user
names.  
**sumd** can also generate reports on remote hosts that uses the
systemd-journal-remote service to send their logs to the host. These
reports only contains information that is extracted from the logs,
The options are as follows:

  ------------------- ------------------------------------------------------
  **-h, --help**      Shows help message  
  **-r, --remote**    Enables the generation of reports for remote hosts.  
  **-s, --service**   Enables running as a service. Sends report via mail.  
  ------------------- ------------------------------------------------------

Requirements
------------
sumd requires the following packages:

* systemd 237-3 or higher

* mailutils 3.4 or higher

* systemd-journal-remote 237-3 or higher

* GNU coreutils 8.28 or higher

Files
-----

These are the file in the sumd package:
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **/usr/local/sbin/sumd**  
  Script file.  
  **/etc/sumd/sumd.conf**  
  Configuration file for sumd. Defines time of day and which days of the week to generate report, default 12:00 every day. Specifies e-mail address to send report to, default is root, and path to remote host logs, default is /var/log/journal/remote/.  
  **/etc/systemd/system/sumd.service**  
  Systemd service file. Defines the service and how it is run.  
  **/var/local/sumd/sumd\_last\_run.var**  
  Contains the date and time for last run.  
  **install.sh**  
  Installation script that installs sumd, configuration and service file, and creates a system user to run the service.  
  **uninstall.sh**  
  Removes everything installed by install.sh.  
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

E-mail setup
------------

To send e-mail sumd requires mailutils because it uses the mail command
in this package. If mailutils is installed with default values, Postfix
will deliver mail to the local users on the host. Postfix can also be
configured as an e-mail server for a valid domain, or as a satelite
system that relays all messages to another SMTP.

Remote logs
-----------

sumd uses the package systemd-journal-remote to get logs from remote
hosts. This package contains several services, but the ones used by sumd
is systemd-journal-remote.service and systemd-journal-upload.service.  
The former is used on the host running the sumd service and by default
sets up a listening service on TCP port 19532 that listens for HTTPS
connections. This requires that you have certificates to use for HTTPS.  
The systemd-journal-upload.service is installed on the hosts you want to
get logs from and by default it also uses HTTPS, and you have to define
the URL to the sumd server in its configuration file.

Output
------
Explanation of output:
  ---------------------------------------------------------------------------------------------------------------------
  **Number of SSH connections**  
  All SSH connections made to host, including connections rejected because of invalid user or no matching algorithms.  
  **Number of authentication failures**  
  Authentication failures from all systems using PAM: SSH, sudo, samba etc.  
  **Number of logins**  
  Number of successful logins from SSH or local.  
  **Number of users**  
  Number of user that has logged in.  
  **Users logged in**  
  Which users that has logged in. This is not sent in the e-mail report.  
  **Uptime**  
  Time since last boot.  
  **Memory usage**  
  Shows current memory usage when run as command line tool, and peak usage in time range when run as service.  
  **Current disk usage**  
  Disk usage at time of report generation.  
  ---------------------------------------------------------------------------------------------------------------------

Supported platforms
-------------------

sumd has been tested with Ubuntu Server 18.04.1 and Xubuntu 18.04.1. It
should be compatible with all Ubuntu based distributions.

License
-------

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.\
This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License at <http://www.gnu.org/licenses/> for more details.
