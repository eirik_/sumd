#!/usr/bin/env bash

error_exit(){
    echo "${PROGNAME}: ${1:-"Unknown error"}" 1>&2
    exit 1
}

if [[ $(id -u) != 0 ]]; then
  error_exit "$LINENO: You must be the superuser to run this script."
fi

echo "Removing script file"
rm /usr/local/sbin/sumd || error_exit "$LINENO: Unable to remove /'usr/local/sbin/sumd'"

echo "Removing sumd.service"
rm /etc/systemd/system/sumd.service || error_exit "$LINENO: Unable to remove '/etc/systemd/system/sumd.service'"

echo "Removing '/etc/sumd/'"
rm -r /etc/sumd || error_exit "$LINENO: Unable to remove 'etc/sumd/'"

echo "Removing '/var/local/sumd'"
rm -r /var/local/sumd || error_exit "$LINENO: Unable to remove '/var/local/sumd'"

echo "Removing user 'sumd'"
deluser -q sumd || error_exit "$LINENO: Unable to remove user 'sumd'"